.PHONY: install
install:
	uv sync --all-extras


.PHONY: format
format:
	uv run ruff format .
	uv run ruff check --fix --exit-zero
	uv run pre-commit run --all-files
	uv run mypy .


.PHONY: test
test:
	uv run pytest --cov=. --cov-report term:skip-covered --cov-report=html .


.PHONY: profiling
profiling:
	uv run python -m cProfile -o profile -m pytest -l
	uv run gprof2dot -f pstats profile | dot -Tpng -o profile.png
